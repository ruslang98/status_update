import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Home',
    component: () => import('../views/StatusTable.vue')
  },
  {
    path: '/sign_in',
    name: 'Log',
    component: () => import('../views/Sign.vue')
  }
]

const router = new VueRouter({
  routes
})

router.beforeEach((to, from, next) => {
  if (to.path !== '/sign_in' && !localStorage.getItem('authorization_token')) next({ name: 'Log' })
  else next()
})

export default router
