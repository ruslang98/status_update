import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify';
import {http} from './http/axios-config.js'
import VueToast from 'vue-toast-notification'
import 'vue-toast-notification/dist/theme-default.css';

Vue.config.productionTip = false
Vue.use(VueToast)

new Vue({
  router,
  store,
  http,
  vuetify,
  render: h => h(App)
}).$mount('#app')
