import {http} from '../../http/axios-config.js'

export default {
    state: {
        token: localStorage.getItem('token') || ''
    },
    mutations:{
        updateToken(state, token){
            state.token = token
        }
    },
    actions:{
        sign_in({commit}, crenditials){
            http.post(`/sign_in/`, crenditials).then(res => {
                localStorage.setItem('token', res.data.token)
                commit('updateToken', res.data.token)
            })
        }
    }
}