import {http} from '../../http/axios-config.js'

export default {
    state: {
        nameProjects: [
            {project_name: 'ST93013', project_id: 1, display_status: 'Выполнено', source_id: 1},
            {project_name: 'ST937413', project_id: 2, display_status: 'Выполнено', source_id: 2},
            {project_name: 'ST930143', project_id: 3, display_status: 'Выполнено', source_id: 3},
            {project_name: 'ST9301523', project_id: 4, display_status: 'Выполнено', source_id: 3},
            {project_name: 'ST31441', project_id: 5, display_status: 'Закрыто', source_id: 3},
            {project_name: 'ST31731', project_id: 6, display_status: 'Отложен', source_id: 3},
            {project_name: 'ST08086', project_id: 7, display_status: 'Просрочен', source_id: 3},
        ],
        typeProjects: [
            {project_type: 'MM',project_id: 1, source_id: 1},
            {project_type: 'MK',project_id: 2, source_id: 2},
            {project_type: 'NT',project_id: 3, source_id: 3},
            {project_type: 'SS',project_id: 4, source_id: 4},
        ],
        statusProjects: [
            {display_status: 'Выполнено'},
            {display_status: 'Закрыто'},
            {display_status: 'Отложен'},
            {display_status: 'Задержан'},
            {display_status: 'Просрочен'},
        ]
    },
    mutations: {
        projectnameUpdate(store, names){
            store.nameProjects = names
        },
        projecttypeUpdate(store, types){
            store.typeProjects = types
        },
        projectstatusUpdate(store, statuses){
            store.statusProjects = statuses
        }
    },
    actions:{
        getProjectName(ctx){
            return new Promise((resolve, reject) => {
                http.get('/projectnames/')
                .then((res) => {
                    ctx.commit('projectnameUpdate', res.data)
                    resolve()
                })
                .catch(() => {
                    reject()
                })
            })
        },
        getProjectType(ctx){
            return new Promise((resolve, reject) => {
                http.get('/projecttypes/')
                .then((res) => {
                    ctx.commit('projecttypeUpdate', res.data)
                    resolve()
                })
                .catch(() => {
                    reject()
                })
            })
        },
        getProjectStatuses(ctx){
            return new Promise((resolve, reject) => {
                http.get('/projectstatuses/')
                .then((res) => {
                    ctx.commit('projectstatusUpdate', res.data)
                    resolve()
                })
                .catch(() => {
                    reject()
                })
            })
        },
        filterData(ctx, crenditials){
            return new Promise((resolve, reject) => {
                console.log(crenditials)
                http.get(`/projects/`, {
                    params: crenditials
                })
                .then((res) => {
                    ctx.commit('projectsUpdate', res.data)
                    resolve(res)
                })
                .catch((err) => {
                    reject(err)
                })
            })
        }
    },
    getters: {
        projectName(state){
            return state.nameProjects
        },
        typeProjects(state){
            return state.typeProjects
        },
        statusProjects(state){
            return state.statusProjects
        },
        projectNameFiltered: state => ( id, status) => {
            return state.nameProjects.filter(item => item.source_id == id).filter(item => item.display_status == status)
        },
        projectTypeFiltered: state => id => {
            return state.nameProjects.filter(item => item.source_id == id)
        }
    }
}