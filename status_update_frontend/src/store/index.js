import Vue from 'vue'
import Vuex from 'vuex'
import Request from './modules/Requests.js'


Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Request
  }
})

