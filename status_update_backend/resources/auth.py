from flask_restful import Resource, request
from flask_jwt_extended import create_access_token

from serializers import user_schemas

from models import User

from db import db


class SignupAPI(Resource):

    def post(self):
        username = request.json['username']
        password = request.json['password']
        new_user = User(username, password)
        new_user.hash_password()
        db.session.add(new_user)
        db.session.commit()
        return {'id': new_user.id,
                'username': new_user.username,
                'password': new_user.password}, 200


class LoginAPI(Resource):
    def post(self):
        username = request.json['username']
        password = request.json['password']
        user = User.query.filter_by(username=username).first()
        authorized = user.check_password(password)
        if not authorized:
            return {'error': 'email or password invalid'}, 401
        access_token = create_access_token(identity=str(user.id))
        return {'id': user.id,
                'token': access_token}, 200


class UserListAPI(Resource):
    def get(self):
        all_users = User.query.all()
        result = user_schemas.dump(all_users)
        return user_schemas.jsonify(result)
