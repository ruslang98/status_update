from datetime import datetime

from flask import jsonify
from flask_restful import Resource, request
from flask_restful_swagger import swagger


from db import db
from models import Project
from serializers import project_schema, projects_schema


class ProjectListAPI(Resource):
    """Endpoint to GET or POST projects."""
    @swagger.operation(
        notes='GET list of projects.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            }
        ]
    )
    def get(self):
        """Returns list of filter projects."""
        global filter_result
        query_project_title = request.args.get('project_title', '')
        query_affiliate = request.args.get('affiliate', '')
        query_form = request.args.get('form', '')
        query_status = request.args.get('status', '')
        query_start_date = request.args.get('start_date', '')
        query_end_date = request.args.get('end_date', '')

        if query_project_title and query_affiliate and query_form \
                and query_form and query_start_date and query_end_date == '':
            all_projects = Project.query.all()
            result = projects_schema.dump(all_projects)
            return projects_schema.jsonify(result)
        else:
            project_filters = {'affiliate': query_affiliate, 'status': query_status,
                               'project_title': query_project_title, 'form': query_form}

            project_filters = {key: value for key, value in project_filters.items() if value != ''}

            if query_start_date and query_end_date:
                filter_result = Project.query.filter_by(**project_filters).\
                    filter((Project.date >= query_start_date) & (Project.date <= query_end_date)).all()

            elif not query_start_date and query_end_date:
                filter_result = Project.query.filter_by(**project_filters).filter(Project.date <= query_end_date).all()

            elif query_start_date and not query_end_date:
                filter_result = Project.query.filter(**project_filters).filter(Project.date >= query_start_date).all()

            elif not query_start_date and not query_end_date:
                filter_result = Project.query.filter_by(**project_filters).all()

            return projects_schema.jsonify(filter_result)

    @swagger.operation(
        notes='POST to add project.',
        parameters=[
            {
                "name": "body",
                "description": "Date format : YYYY-MM-DD",
                "required": True,
                "allowMultiple": False,
                "dataType": Project.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 201,
                "message": "Created."
            },
            {
                "code": 400,
                "message": "Bad request."
            }
        ],

    )
    def post(self):
        """Create new project."""
        affiliate = request.json['affiliate']
        project_title = request.json['project_title']
        form = request.json['form']
        status = request.json['status']
        date = datetime.strptime(request.json['date'], "%Y-%m-%d")

        new_project = Project(affiliate, project_title, form, status, date)

        db.session.add(new_project)
        db.session.commit()
        return jsonify({'id': new_project.id,
                        'affiliate': new_project.affiliate,
                        'project_title': new_project.project_title,
                        'form': new_project.form,
                        'status': new_project.status,
                        'date': new_project.date})


class ProjectDetailAPI(Resource):
    """Endpoint to GET, PATCH, DELETE project by id."""
    @swagger.operation(
        notes='GET project by id.',
        responseMessages=[
            {
                "code": 200,
                "message": "OK"
            }
        ]
    )
    def get(self, id):
        """Returns details of a project."""
        project = Project.query.get_or_404(id)
        return project_schema.jsonify(project)

    @swagger.operation(
        notes='Update project by id.',
        parameters=[
            {
                "name": "body",
                "description": "You can change only date and status. Date format : YYYY-MM-DD ",
                "required": True,
                "allowMultiple": False,
                "dataType": Project.__name__,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "OK."
            },
            {
                "code": 400,
                "message": "Bad request."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def patch(self, id):
        """Update project."""
        project = Project.query.get_or_404(id)
        if 'status' in request.json:
            project.status = request.json['status']
        db.session.commit()
        return project_schema.jsonify(project)

    @swagger.operation(
        notes='Delete project by id.',
        responseMessages=[
            {
                "code": 204,
                "message": "Deleted."
            },
            {
                "code": 404,
                "message": "Not found."
            }
        ]
    )
    def delete(self, id):
        """Delete project."""
        project = Project.query.get_or_404(id)
        db.session.delete(project)
        db.session.commit()
        return '', 204


class ProjectListStatusesUpdateAPI(Resource):
    """Endpoint to change status for list of projects"""

    @swagger.operation(
        notes='some really good notes',
        nickname='upload',
        parameters=[
            {
                "name": "body",
                "description": "{}",
                "required": True,
                "allowMultiple": False,
                "paramType": "body"
            }
        ],
        responseMessages=[
            {
                "code": 200,
                "message": "OK"
            },
            {
                "code": 404,
                "message": "Not found"
            }
        ]
    )
    def post(self):
        list_id = request.json['id']
        new_status = request.json['status']
        for id in list_id:
            project = Project.query.get_or_404(id)
            project.status = new_status
        db.session.commit()
        return jsonify({'id': list_id,
                        'status': new_status})






