from db import db

from flask_restful_swagger import swagger
from flask_bcrypt import generate_password_hash, check_password_hash

@swagger.model
class Project(db.Model):
    """Mass update statuses projects model"""

    id = db.Column(db.Integer, primary_key=True, name='project_id')
    affiliate = db.Column(db.String(256), nullable=False)
    project_title = db.Column(db.String(256), nullable=False)
    form = db.Column(db.String(256), nullable=False)
    status = db.Column(db.String(256), nullable=False)
    date = db.Column(db.Date, nullable=False)

    def __init__(self, affiliate, project_title, form, status, date):
        self.affiliate = affiliate
        self.project_title = project_title
        self.form = form
        self.status = status
        self.date = date

    def __repr__(self):
        return '<Project> %s' % self.project_title

    def save_to_db(self):
        db.session.add(self)
        db.session.commit()


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(256), unique=True, nullable=False)
    password = db.Column(db.String(256), unique=True, nullable=False)

    def __init__(self, username, password):
        self.username = username
        self.password = password

    def hash_password(self):
        self.password = generate_password_hash(self.password).decode('utf-8')

    def check_password(self, password):
        return check_password_hash(self.password, password)


