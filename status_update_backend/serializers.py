from flask_marshmallow import Marshmallow

ma = Marshmallow()


class ProjectSchema(ma.Schema):

    class Meta:
        fields = ('id', 'affiliate', 'project_title', 'form', 'status', 'date')


project_schema = ProjectSchema()
projects_schema = ProjectSchema(many=True)


class UserSchema(ma.Schema):

    class Meta:
        fields = ('id', 'username', 'password')


user_schema = UserSchema()
user_schemas = UserSchema(many=True)


