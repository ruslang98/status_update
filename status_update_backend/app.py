import os

from flask import Flask
from flask_bcrypt import Bcrypt
from flask_cors import CORS
from flask_restful import Api
from flask_restful_swagger import swagger
from flask_jwt_extended import JWTManager

from db import db
from resources.projects import (
    ProjectListAPI,
    ProjectDetailAPI,
    ProjectListStatusesUpdateAPI
)

from resources.auth import SignupAPI, LoginAPI, UserListAPI

app = Flask(__name__)

CORS(app)

basedir = os.path.abspath(os.path.dirname(__file__))
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, 'db.sqlite3')
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['JWT_SECRET_KEY'] = 't1NP63m4wnBg6nyHYKfmc2TpCOGI4nss'

api = swagger.docs(Api(app), apiVersion='0.1', api_spec_url='/api/docs')
bcrypt = Bcrypt(app)
jwt = JWTManager(app)


@app.before_first_request
def create_tables():
    db.create_all()


db.init_app(app)

api.add_resource(SignupAPI, '/auth/signup/')
api.add_resource(LoginAPI, '/auth/signin/')

api.add_resource(ProjectListAPI, '/projects/')
api.add_resource(ProjectDetailAPI, '/projects/<id>/')
api.add_resource(ProjectListStatusesUpdateAPI, '/projects/status/')

api.add_resource(UserListAPI, '/users/')
